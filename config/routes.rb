Rails.application.routes.draw do
  resources :games, except: [:update, :destroy] do
    patch "/", to: "games#make_move"
  end
end
