require "rails_helper"

RSpec.describe GamesController, type: :controller do
  let!(:u2) { User.create!(email: "other_email@email.com", password: "hash3d_passw0rd") }
  let(:headers) { { Authorization: u2.email } }

  describe "index" do
    let(:parsed_response) { JSON.parse(response.body) }
    before do
      u = User.create!(email: "some_email@email.com", password: "hash3d_passw0rd")
      5.times { Game.create_from(rows: 10, columns: 10, mines: 10, user: u).save }
      3.times { Game.create_from(rows: 10, columns: 10, mines: 10, user: u2).save }
      request.headers.merge! headers
      get :index
    end

    it "lists all games for the given user" do
      expect(parsed_response.length).to eq(3)
    end

    it "returns only ids" do
      # that's all we're showing on the games index page
      expect(parsed_response.map { |game| game["id"] }).to eq(u2.games.map(&:id))
    end
  end

  describe "create" do
    let(:params) { {
      rows: 10,
      columns: 10,
      mines: 8,
      user: u2.email
    } }

    before do
      Game.delete_all
      request.headers.merge! headers
      post :create, params: params
    end

    it "creates a game" do
      expect(Game.count).to eq(1)
    end
  end

  describe "show" do
  end

  describe "make move" do
  end
end
