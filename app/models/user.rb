class User < ApplicationRecord
  has_many :games
  validates :email, presence: true, uniqueness: true
  validates :password, presence: true
end
