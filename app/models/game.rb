class Game < ApplicationRecord
  # Each matrix will look something like this, as well as containing flagged + clicked info
  # on each cell
  #
  # 1 1 2 1 1 0
  # 1 B 3 B 1 0
  # 1 2 B 2 1 0
  # 0 1 1 1 1 1
  # 0 0 0 0 1 B
  #
  FLAG_CELL = "flag"
  CLICK_CELL = "click"

  attr_accessor :cells
  serialize :cells, Array

  belongs_to :user

  def self.create_from(rows:, columns:, mines:, user:)
    matrix = Matrix.build(rows, columns) { |_r, _c| { :flagged => false, :clicked => false, :value => 0 } }
    game = Game.new(user: user, cells: matrix)
    game.populate_matrix!(rows, columns, mines)
    game
  end

  def make_move!(cell, type)
    # type = click or flag
    if type == Game::FLAG_CELL
      flag_cell(cell)
    elsif type == Game::CLICK_CELL
      click_cell(cell)
    end
  end

  def populate_matrix!(rows, columns, mines)
    # generate bombs coords
    coords = Set.new
    while coords.size < mines
      coords << [
        rand(rows),
        rand(columns)
      ]
    end

    # set them
    coords.each do |c|
      cells[c[0], c[1]][:value] = "B"
      increment_neighbors(c)
    end
  end

  def won?
    !lost && non_bomb_cells.all? { |c| c[:clicked] }
  end

  def lost?
    cells.any? { |c| c[:value] == "B" && c[:clicked] }
  end

  def to_s
    # display bomb counts on matrix, for development purposes
    cells.to_a.each do |row|
      print row.map { |cell| cell[:value] }.join("  ")
      print "\n"
    end
  end

  def to_status
    # display clicked status on matrix, for development purposes
    cells.to_a.each do |row|
      print row.map { |cell| cell[:clicked] }.join("  ")
      print "\n"
    end
  end

  private

  def click_cell(c)
    target = cells[c[0], c[1]]
    return if target[:clicked] || target[:flagged]

    cells[c[0], c[1]][:clicked] = true
    if target[:value] == 0
      # reveal 0 neighbors
      do_on_neighbors_of(c) do |target_x, target_y|
        target = cells[target_x, target_y]
        if target[:value] == 0
          click_cell([target_x, target_y])
        end
      end
    end
  end

  def do_on_neighbors_of(c)
    [-1, 0, 1].each do |x|
      [-1, 0, 1].each do |y|
        target_x = c[0] + x
        target_y = c[1] + y
        yield target_x, target_y unless (x == 0 && y == 0) || out_of_range?(target_x, target_y)
      end
    end
  end

  def flag_cell(c)
    cells[c[0], c[1]][:flagged] = !cells[c[0], c[1]][:flagged]
  end

  def get_cell(c)
    cells[c[0], c[1]]
  end

  def increment_neighbors(c)
    # after adding a bomb on c, accomodate bomb count of neighbors
    do_on_neighbors_of(c) do |target_x, target_y|
      unless is_bomb?(target_x, target_y)
        cells[target_x, target_y][:value] += 1
      end
    end
  end

  def is_bomb?(x, y)
    cells[x, y][:value] == "B"
  end

  def non_bomb_cells
    cells.reject { |c| c[:value] == "B" }
  end

  def out_of_range?(x, y)
    x < 0 || y < 0 || cells[x, y] == nil
  end
end
