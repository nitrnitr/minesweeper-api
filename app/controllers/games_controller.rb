class GamesController < ApplicationController
  def index
    # List all games for the logged user
    games = Game.where(user: user)
    render json: games, each_serializer: GameIndexSerializer
  end

  def create
    # Start new game
    game = Game.create_from(rows: create_params[:rows].to_i,
                            columns: create_params[:columns].to_i,
                            mines: create_params[:mines].to_i,
                            user: user)

    if game.save
      render json: game.id, status: :ok
    else
      render json: game.errors, status: :unprocessable_entity
    end
  end

  def show
    # Return json details for the game load
    render json: game
  end

  def make_move
    # Make move on game, return updated game
    game.make_move!(params[:cell], params[:type])
    render json: game
  end

  private

  def create_params
    params.permit(:rows, :columns, :mines)
  end

  def game
    @game ||= Game.find(params.require(:id))
  end

  def user
    # for the purpose of this exercise the header will contain the user email
    # instead of a token
    email = request.headers["Authorization"]
    @user ||=  User.find_by(email: email)
    unless @user
      # temporary for development purposes :)
      puts "User not found!"
      @user = User.first
    end
    @user
  end
end
