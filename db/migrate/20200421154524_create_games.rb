class CreateGames < ActiveRecord::Migration[5.1]
  def change
    create_table :games do |t|
      t.references :user, foreign_key: true, null: false
      t.text :cells

      t.timestamps
    end
  end
end
