Endpoints:
- GET /games - list of games for the logged user
- POST /games - start a new game
- GET /games/:id - show details of :id game
- PATCH /games/:id - make a movement on a game

What's missing:
- Endpoints to register users & login. For that reason, we're assuming the request comes in with a Authorization header that includes the user email, instead of a token as it should be.
- Model Tests, more controller tests
- Somehow return if game is won/lost to Frontend (model logic is there already)
- Input checking (cells size, mines amount less than width x height, etc)

Assumptions:
- Frontend creates game with width, height, mines, and user. Game ID is returned, FE should redirect to /games/:id
- After making a movement, whole updated game is returned to the Frontend for re-drawing
- Each cell contains data about its value (Bomb, or amount of bomb neigbors, Flaged/Not Flagged, Clicked/Not Clicked)

Possible improvements:
- Only return modified cells when making a movement
